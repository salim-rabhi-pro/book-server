package com.tuto.book.model;

import com.google.protobuf.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sun.print.resources.serviceui_zh_TW;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;




/**
 * Created by salim on 26 Jun, 2020
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Author implements Serializable {

    private static final long serialVersionUID = -2738075714789324612L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String firstName;
    private String lastName;
    private Timestamp birthDate;
    private String email;
    @Enumerated(value = EnumType.ORDINAL)
    @ElementCollection(fetch = FetchType.EAGER)
    private List<Domain> domains;
    @OneToMany(fetch = FetchType.EAGER)
    Set<Book> books;

    public enum Domain {
        UNKNOWN(-1),
        FLE7A(0),
        ASTRONOMY(1),
        ART(2),
        SCIENCE(3),
        HISTORY(4);

        private final int value;
        Domain(int value){ this.value= value;}

        public int getValue(){return value;}
        public static Domain getDomainByValue(int value){
            switch (value){
                case 0: return FLE7A;
                case 1: return ASTRONOMY;
                case 2: return ART;
                case 3: return SCIENCE;
                case 4: return HISTORY;
                default:return UNKNOWN;
            }
        }
    }
}
