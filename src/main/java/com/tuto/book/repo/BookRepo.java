package com.tuto.book.repo;

import com.tuto.book.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by salim on 26 Jun, 2020
 */
@Repository
public interface BookRepo extends JpaRepository<Book, Long> {
    Optional<Book> findById(Long id);
}
