package com.tuto.book.server;

import com.tuto.book.grpc.server.*;
import com.tuto.book.model.Book;
import com.tuto.book.repo.AuthorRepo;
import com.tuto.book.repo.BookRepo;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by salim on 23 Jun, 2020
 */
@GrpcService
public class BookServiceImpl extends BookServiceGrpc.BookServiceImplBase {
    @Autowired
    private BookRepo bookRepo;
    @Autowired
    private AuthorRepo authorRepo;

    @Override
    public void ping(PingRequest request, StreamObserver<PongResponse> responseObserver) {
        String pong = "pong";
        PongResponse response = PongResponse.newBuilder()
                .setPong(pong)
                .build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void addBook(AddBookRequest request, StreamObserver<AddBookResponse> responseObserver) {
        List<com.tuto.book.model.Author.Domain> domains = new ArrayList<>();
        com.tuto.book.grpc.server.Book serverBook = request.getBook();
        Author serverAuthor = request.getBook().getAuthor();
        for (Domain domain : serverAuthor.getDomainsList())
            domains.add(com.tuto.book.model.Author.Domain.getDomainByValue(domain.getNumber()));
        com.tuto.book.model.Author author = com.tuto.book.model.Author.builder()
                .firstName(serverAuthor.getFirstName())
                .lastName(serverAuthor.getLastName())
                .birthDate(serverAuthor.getBirthDate())
                .email(serverAuthor.getEmail())
                .domains(domains)
                .build();
        com.tuto.book.model.Author authorModel = authorRepo.save(author);

        Book bookResult = bookRepo.save(Book.BookBuilder.aBook()
                .setTitle(serverBook.getTitle())
                .setIsbn(serverBook.getIsbn())
                .setPublishingDate(serverBook.getPublishingDate())
                .setAuthor(authorModel)
                .build());
        AddBookResponse addBookResponse = AddBookResponse.newBuilder()
                .setId(bookResult.getId())
                .build();
        responseObserver.onNext(addBookResponse);
        responseObserver.onCompleted();
    }

    @Override
    public void getBook(GetBookRequest request, StreamObserver<GetBookResponse> responseObserver) {
        Long id  = request.getId();
        Optional<Book> bookOptional = bookRepo.findById(id);
        System.out.println("aaa");
        if (!bookOptional.isPresent()){
            return ;
        }
        Book book = bookOptional.get();
        com.tuto.book.model.Author author = book.getAuthor();
        List<Domain> serverDomains = new ArrayList<>();
        author.getDomains().forEach(x -> serverDomains.add(Domain.forNumber(x.getValue())));
        Author authorServer = Author.newBuilder()
                .setId(author.getId())
                .setFirstName(author.getFirstName())
                .setLastName(author.getLastName())
                .setBirthDate(author.getBirthDate())
                .addAllDomains(serverDomains)
                .setEmail(author.getEmail())
                .build();
        com.tuto.book.grpc.server.Book bookServer = com.tuto.book.grpc.server.Book.newBuilder()
                .setId(book.getId())
                .setTitle(book.getTitle())
                .setIsbn(book.getIsbn())
                .setPublishingDate(book.getPublishingDate())
                .setAuthor(authorServer)
                .build();
        GetBookResponse getBookResponse = GetBookResponse.newBuilder().setBook(bookServer).build();

        responseObserver.onNext(getBookResponse);
        responseObserver.onCompleted();
    }
}
