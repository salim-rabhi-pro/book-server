package com.tuto.book.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.protobuf.Timestamp;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by salim on 26 Jun, 2020
 */
@Data
@Entity
@ToString
public class Book implements Serializable {
    private static final long serialVersionUID = 8777420871318139537L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String title;
    private String isbn;
    private Timestamp publishingDate;
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "author_id", referencedColumnName = "id", nullable = false)
    private Author author;

    public static final class BookBuilder {
        private Long id;
        private String title;
        private String isbn;
        private Timestamp publishingDate;
        private Author author;

        private BookBuilder() {
        }

        public static BookBuilder aBook() {
            return new BookBuilder();
        }

        public BookBuilder setId(Long id) {
            this.id = id;
            return this;
        }

        public BookBuilder setTitle(String title) {
            this.title = title;
            return this;
        }

        public BookBuilder setIsbn(String isbn) {
            this.isbn = isbn;
            return this;
        }

        public BookBuilder setPublishingDate(Timestamp publishingDate) {
            this.publishingDate = publishingDate;
            return this;
        }

        public BookBuilder setAuthor(Author author) {
            this.author = author;
            return this;
        }

        public Book build() {
            Book book = new Book();
            book.setId(id);
            book.setTitle(title);
            book.setIsbn(isbn);
            book.setPublishingDate(publishingDate);
            book.setAuthor(author);
            return book;
        }
    }
}
