package com.tuto.book.repo;

import com.tuto.book.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by salim on 09 Jul, 2020
 */
@Repository
public interface AuthorRepo extends JpaRepository<Author, Long> {
}
