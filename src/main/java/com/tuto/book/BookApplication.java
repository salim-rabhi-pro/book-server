package com.tuto.book;

import com.google.protobuf.Timestamp;
import com.tuto.book.model.Author;
import com.tuto.book.model.Book;
import com.tuto.book.repo.AuthorRepo;
import com.tuto.book.repo.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Optional;

@SpringBootApplication
public class BookApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(BookApplication.class, args);
	}

	@Autowired
	BookRepo bookRepo;

	@Autowired
	AuthorRepo authorRepo;
	@Override
	public void run(String... args) throws Exception {
		saveBookWithId2();
		Optional<Book> bookOptional = bookRepo.findById(1L);
		System.out.println("aaaaaaaa");
		if (bookOptional.isPresent())
			System.out.println(bookOptional.get());
	}

	private void saveBookWithId2() {
		LocalDateTime localDateTime = LocalDateTime.parse("2011-12-03T10:15:30", DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		
		Instant instant = localDateTime.atZone(ZoneOffset.UTC).toInstant();
		Timestamp publishingDateAsTS = Timestamp.newBuilder().setSeconds(instant.getEpochSecond())
				.setNanos(instant.getNano()).build();

		localDateTime = LocalDateTime.parse("1984-11-13T14:07:59", DateTimeFormatter.ISO_LOCAL_DATE_TIME);

		Instant instant2 = localDateTime.atZone(ZoneOffset.UTC).toInstant();
		Timestamp birthDateAsTS = Timestamp.newBuilder().setSeconds(instant.getEpochSecond())
				.setNanos(instant.getNano()).build();

		Author authorCreated = authorRepo.save(Author.builder()
						.id(2L)
				.firstName("foo")
				.lastName("bar")
				.domains(Arrays.asList(Author.Domain.SCIENCE, Author.Domain.ASTRONOMY))
				.email("foobat@email.com")
				.birthDate(birthDateAsTS)
				.build());
		Book savedBook = bookRepo.save(Book.BookBuilder.aBook()
				.setIsbn("book2 ISBN")
				.setPublishingDate(publishingDateAsTS)
				.setTitle("tiiitle")
				.setId(2L)
				.setAuthor(authorCreated)
				.build());
		System.out.println(savedBook.getId());
	}
}
